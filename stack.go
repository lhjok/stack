package stack

type Stack struct {
	size             int
	currentPage      []interface{}
	pages            [][]interface{}
	offset           int
	capacity         int
	pageSize         int
	currentPageIndex int
}

const s_DefaultAllocPageSize = 4096

func NewStack() *Stack {
	stack := new(Stack)
	stack.currentPage = make([]interface{}, s_DefaultAllocPageSize)
	stack.pages = [][]interface{}{stack.currentPage}
	stack.offset = 0
	stack.capacity = s_DefaultAllocPageSize
	stack.pageSize = s_DefaultAllocPageSize
	stack.size = 0
	stack.currentPageIndex = 0

	return stack
}

func NewStackWithCapacity(cap int) *Stack {
	stack := new(Stack)
	stack.currentPage = make([]interface{}, cap)
	stack.pages = [][]interface{}{stack.currentPage}
	stack.offset = 0
	stack.capacity = cap
	stack.pageSize = cap
	stack.size = 0
	stack.currentPageIndex = 0

	return stack
}

func (s *Stack) Push(elem ...interface{}) {
	if elem == nil || len(elem) == 0 {
		return
	}

	if s.size == s.capacity {
		pages_count := len(elem) / s.pageSize
		if len(elem)%s.pageSize != 0 {
			pages_count++
		}
		s.capacity += s.pageSize

		s.currentPage = make([]interface{}, s.pageSize)
		s.pages = append(s.pages, s.currentPage)
		s.currentPageIndex++

		pages_count--
		for pages_count > 0 {
			page := make([]interface{}, s.pageSize)
			s.pages = append(s.pages, page)
		}

		s.offset = 0
	}

	available := len(s.currentPage) - s.offset
	for len(elem) > available {
		copy(s.currentPage[s.offset:], elem[:available])
		s.currentPage = s.pages[s.currentPageIndex+1]
		s.currentPageIndex++
		elem = elem[available:]
		s.offset = 0
		available = len(s.currentPage)
	}

	copy(s.currentPage[s.offset:], elem)
	s.offset += len(elem)
	s.size += len(elem)
}

func (s *Stack) Pop() (elem interface{}) {
	if s.size == 0 {
		return nil
	}

	s.offset--
	s.size--
	if s.offset < 0 {
		s.offset = s.pageSize - 1

		s.currentPage, s.pages = s.pages[len(s.pages)-2], s.pages[:len(s.pages)-1]
		s.capacity -= s.pageSize
		s.currentPageIndex--
	}

	elem = s.currentPage[s.offset]

	return
}

func (s *Stack) Top() (elem interface{}) {
	if s.size == 0 {
		return nil
	}

	off := s.offset - 1
	if off < 0 {
		page := s.pages[len(s.pages)-1]
		elem = page[len(page)-1]
		return
	}
	elem = s.currentPage[off]
	return
}

func (s *Stack) Size() int {
	return s.size
}
